package com.elrsoft.workflow.db.async;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.elrsoft.workflow.fragments.MainFragment;
import com.elrsoft.workflow.model.Task;
import com.elrsoft.workflow.service.TaskService;

import java.util.List;

/**
 * Created by Alex on 03.04.2015.
 */
public class MainFragmentAsync extends AsyncTask<Object, List<Task>, List<Task>> {

    private ProgressDialog waitingDialog;
    private Context context;
    MainFragment mainFragment;

    public MainFragmentAsync(MainFragment mainFragment){
        this.mainFragment = mainFragment;
        this.context = mainFragment.getActivity();
    }

    @Override
    protected void onPreExecute() {
        waitingDialog = ProgressDialog.show(context, "З'єднання...", "Під'єднюємось до бази данних...", true);
    }

    @Override
    protected void onPostExecute(List<Task> taskList) {
        mainFragment.updateAdapter(taskList);
        waitingDialog.dismiss();
    }

    @Override
    protected  List<Task> doInBackground(Object... params) {
        return new TaskService(context).getAll();
    }

}
