package com.elrsoft.workflow.core;

/**
 * Created by Alex on 27.03.2015.
 */
public interface Duplicatable {
    boolean isDuplicat();
}
