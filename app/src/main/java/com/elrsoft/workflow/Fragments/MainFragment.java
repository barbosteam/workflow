package com.elrsoft.workflow.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;

import com.elrsoft.workflow.R;
import com.elrsoft.workflow.adapter.FilterAdapter;
import com.elrsoft.workflow.controller.AnimButtonCreatePostController;
import com.elrsoft.workflow.controller.ToastManager;
import com.elrsoft.workflow.db.async.MainFragmentAsync;
import com.elrsoft.workflow.fragments.core.Initilization;
import com.elrsoft.workflow.fragments.core.Listeners;
import com.elrsoft.workflow.fragments.core.Transaction;
import com.elrsoft.workflow.model.Task;
import com.elrsoft.workflow.service.TaskService;
import com.gc.materialdesign.views.ButtonFloat;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Alex on 31.03.2015.
 */
public class MainFragment extends Fragment implements Initilization, Listeners {

    private FilterAdapter filterAdapter;
    private List<Task> taskList = new ArrayList<>();
    private RecyclerView recyclerView;
    private View view, listEmpty;
    private AnimButtonCreatePostController animButtonCreatePostController;
    private ButtonFloat createTask;
    private LinkedHashMap<String, String> param;
    private  Task task;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_main, null, false);
        initializationView();
        initializationListeners();
        new MainFragmentAsync(this).execute();
        return view;
    }

    public void updateAdapter(List<Task> list) {

        if (param != null) {
            taskList = new TaskService(getActivity()).getFilterList(param);
        } else {
            taskList = list;
        }

        if (taskList.size() > 0) {
            filterAdapter = new FilterAdapter(this, taskList);
            recyclerView.setAdapter(filterAdapter);
            listEmpty.setVisibility(View.INVISIBLE);
        } else {
            listEmpty.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void initializationView() {
        recyclerView = (RecyclerView) view.findViewById(R.id.task_list);
        listEmpty = view.findViewById(R.id.list_empty);
        createTask = (ButtonFloat) view.findViewById(R.id.create_task);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void initializationListeners() {
        createTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Transaction.showFragment((ActionBarActivity) getActivity(), new TaskCreateFragment());
            }
        });

        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            Animation animation = null;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 1 && recyclerView.getScrollState() == 2) {
                    animButtonCreatePostController.startAnim(AnimationUtils.loadAnimation(getActivity(),
                            R.anim.anim_button_create_post_out), View.GONE);
                } else if (dy < 1 && recyclerView.getScrollState() == 2) {
                    animButtonCreatePostController.startAnim(AnimationUtils.loadAnimation(getActivity(),
                            R.anim.anim_button_create_post_in), View.VISIBLE);
                }
            }
        });
    }

    public static MainFragment getInstance(LinkedHashMap<String, String> param) {
        MainFragment fragment = new MainFragment();
        fragment.param = param;
        return fragment;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        FilterAdapter adapter = (FilterAdapter) recyclerView.getAdapter();
       // Task task  = adapter.getTaskId(acmi.position);
        ToastManager.show(getActivity(), String.valueOf(adapter.getPosition()));
        switch (item.getItemId()) {
            case 1:

                //Transaction.showFragment((ActionBarActivity)getActivity(), TaskEditFragment.getInstance(task));
                return true;
            case 2:
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}
