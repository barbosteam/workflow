package com.elrsoft.workflow.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.elrsoft.workflow.R;
import com.elrsoft.workflow.model.User;
import com.elrsoft.workflow.service.UserService;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

/**
 * Created by Alex on 22.03.2015.
 */
public class UserEditFragment extends Fragment implements View.OnClickListener {

    public final static String EXTRA_USER = "com.elrsoft.workflow.user";

    private EditText name, email, phone;
    private User user;

    public static UserEditFragment getInstance(User user) {
        UserEditFragment fragment = new UserEditFragment();
        Bundle args = new Bundle();
        String[] mass = {user.getId()+"", user.getName(), user.getEmail(), user.getPhone()};
        args.putStringArray(EXTRA_USER, mass);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String[] mass = getArguments().getStringArray(EXTRA_USER);
        user = new User(Long.valueOf(mass[0]), mass[1], mass[2], mass[3]);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_edit, container, false);
        name = (EditText)view.findViewById(R.id.name);
        email = (EditText)view.findViewById(R.id.email);
        phone = (EditText)view.findViewById(R.id.phone);
        name.setText(user.getName());
        email.setText(user.getEmail());
        phone.setText(user.getPhone());
        Button add = (Button)view.findViewById(R.id.add);
        Button cancel = (Button)view.findViewById(R.id.cancel);
        add.setOnClickListener(this);
        cancel.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add:
                UserService userService = new UserService(getActivity());
                user.setName(name.getText().toString());
                user.setEmail(email.getText().toString());
                user.setPhone(phone.getText().toString());
                userService.update(user);
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.cancel:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }
    }
}
