package com.elrsoft.workflow.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;

import com.elrsoft.workflow.R;
import com.elrsoft.workflow.adapter.drop_down.NameAdapter;
import com.elrsoft.workflow.adapter.drop_down.StateAdapter;
import com.elrsoft.workflow.adapter.drop_down.TopicAdapter;
import com.elrsoft.workflow.controller.ToastManager;
import com.elrsoft.workflow.core.Emptable;
import com.elrsoft.workflow.db.resource.Resource;
import com.elrsoft.workflow.dialog.DateDialog;
import com.elrsoft.workflow.fragments.core.Initilization;
import com.elrsoft.workflow.fragments.core.Listeners;
import com.elrsoft.workflow.fragments.core.Logic;
import com.elrsoft.workflow.fragments.core.Transaction;

import java.util.LinkedHashMap;

/**
 * Created by Alex on 22.03.2015.
 */
public class FilterFragment extends Fragment implements Initilization, Listeners, Logic, Emptable {

    private View view;
    private Button find, cancel;
    private CheckBox nameCheck, topicCheck, stateCheck, dateCheck;
    private RadioButton allTask, filterTask;
    private AutoCompleteTextView name, topic, state, from, to;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_filter, container, false);
        initializationView();
        initializationListeners();
        viewLogic();
        return view;

    }

    @Override
    public void initializationView() {
        find = (Button) view.findViewById(R.id.find);
        cancel = (Button) view.findViewById(R.id.cancel);

        nameCheck = (CheckBox) view.findViewById(R.id.name_check);
        topicCheck = (CheckBox) view.findViewById(R.id.topic_check);
        stateCheck = (CheckBox) view.findViewById(R.id.state_check);
        dateCheck = (CheckBox) view.findViewById(R.id.date_check);

        allTask = (RadioButton) view.findViewById(R.id.all_task);
        filterTask = (RadioButton) view.findViewById(R.id.filter_task);

        name = (AutoCompleteTextView) view.findViewById(R.id.name);
        topic = (AutoCompleteTextView) view.findViewById(R.id.topic);
        state = (AutoCompleteTextView) view.findViewById(R.id.state);
        from = (AutoCompleteTextView) view.findViewById(R.id.from);
        to = (AutoCompleteTextView) view.findViewById(R.id.to);
    }

    @Override
    public void viewLogic() {
        new NameAdapter(getActivity(), name, nameCheck);
        new TopicAdapter(getActivity(), topic, topicCheck);
        new StateAdapter(getActivity(), state, stateCheck);
        new DateDialog(getActivity(), from, getResources().getString(R.string.from));
        new DateDialog(getActivity(), to, getResources().getString(R.string.to));
        setCheckLogic(nameCheck, name);
        setCheckLogic(topicCheck, topic);
        setCheckLogic(stateCheck, state);
        setCheckLogic(dateCheck);
    }

    @Override
    public void initializationListeners() {
        find.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (allTask.isChecked()) {
                    Transaction.showFragment((ActionBarActivity) getActivity(), new MainFragment());
                }
                if (filterTask.isChecked()) {
                    if (!isEmpty()) {
                        LinkedHashMap<String, String> param = new LinkedHashMap<String, String>();
                        if (nameCheck.isChecked()) {
                            param.put(Resource.Task.NAME, name.getText().toString());
                        }
                        if (topicCheck.isChecked()) {
                            param.put(Resource.Task.TOPIC, topic.getText().toString());
                        }
                        if (stateCheck.isChecked()) {
                            param.put(Resource.Task.STATE, state.getText().toString());
                        }
                        if (dateCheck.isChecked()) {
                            param.put("FROM", from.getText().toString());
                            param.put("TO", to.getText().toString());
                        }
                        Transaction.showFragment((ActionBarActivity) getActivity(), MainFragment.getInstance(param));
                    } else
                        ToastManager.show(getActivity(), getString(R.string.empty_field));
                }
            }
        });
    }

    @Override
    public boolean isEmpty() {
        return ((from.getText().toString().equals("") && dateCheck.isChecked()) ||
                (to.getText().toString().equals("") && dateCheck.isChecked()) ||
                (name.getText().toString().equals("") && nameCheck.isChecked()) ||
                (topic.getText().toString().equals("") && topicCheck.isChecked() ||
                (state.getText().toString().equals("") && stateCheck.isChecked())));
    }

    void setCheckLogic(final CheckBox check, final AutoCompleteTextView autoCompleteTextView){
        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check.isChecked()) {
                    filterTask.setChecked(true);
                    autoCompleteTextView.showDropDown();
                } else {
                    autoCompleteTextView.setText("");
                }
                if(!nameCheck.isChecked() &&
                        !topicCheck.isChecked() &&
                        !stateCheck.isChecked() &&
                        !dateCheck.isChecked())
                    allTask.setChecked(true);
            }
        });
    }

    void setCheckLogic(final CheckBox check){
        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check.isChecked()) {
                    filterTask.setChecked(true);
                }
            }
        });
    }

}
