package com.elrsoft.workflow.fragments.core;

import android.graphics.Color;

import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;

/**
 * Created by Alex on 05.04.2015.
 */
public class EditParam {

    public static void setParam(MaterialAutoCompleteTextView materialAutoCompleteTextView, Integer max){
        materialAutoCompleteTextView.setPrimaryColor(Color.parseColor("#FF00897B"));
        materialAutoCompleteTextView.setFloatingLabel(2);
        if (max != null)
        materialAutoCompleteTextView.setMaxCharacters(max);
        //materialAutoCompleteTextView.validateWith(new RegexpValidator("Only Integer Valid!", "\\d+"));
        materialAutoCompleteTextView.setSingleLineEllipsis(true);
    }

}
