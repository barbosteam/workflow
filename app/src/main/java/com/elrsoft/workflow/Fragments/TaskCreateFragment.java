package com.elrsoft.workflow.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.elrsoft.workflow.R;
import com.elrsoft.workflow.activity.MainActivity;
import com.elrsoft.workflow.adapter.drop_down.NameAdapter;
import com.elrsoft.workflow.adapter.drop_down.TopicAdapter;
import com.elrsoft.workflow.controller.ToastManager;
import com.elrsoft.workflow.core.Emptable;
import com.elrsoft.workflow.dialog.DateDialog;
import com.elrsoft.workflow.fragments.core.EditParam;
import com.elrsoft.workflow.fragments.core.Initilization;
import com.elrsoft.workflow.fragments.core.Listeners;
import com.elrsoft.workflow.fragments.core.Logic;
import com.elrsoft.workflow.fragments.core.Transaction;
import com.elrsoft.workflow.model.Task;
import com.elrsoft.workflow.model.resourse.State;
import com.elrsoft.workflow.service.TaskService;
import com.gc.materialdesign.views.ButtonRectangle;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;

/**
 * Created by Alex on 22.03.2015.
 */
public class TaskCreateFragment extends Fragment implements Initilization, Logic, Listeners, Emptable {

    private View view;
    private MaterialAutoCompleteTextView name, dateComplete, topic, comment;
    private ButtonRectangle save, cancel;
    private NameAdapter nameAdapter;
    private int position;

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_task_create, container, false);
        ((MainActivity)getActivity()).getSupportActionBar().setTitle(R.string.task_create);
        initializationView();
        initializationListeners();
        viewLogic();
        return view;

    }

    @Override
    public void initializationView() {
        name = (MaterialAutoCompleteTextView) view.findViewById(R.id.name);
        dateComplete = (MaterialAutoCompleteTextView) view.findViewById(R.id.date_complete);
        topic = (MaterialAutoCompleteTextView) view.findViewById(R.id.topic);
        comment = (MaterialAutoCompleteTextView) view.findViewById(R.id.comment);

        save = (ButtonRectangle) view.findViewById(R.id.save);
        cancel = (ButtonRectangle) view.findViewById(R.id.cancel);
    }

    @Override
    public void initializationListeners() {

        name.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TaskCreateFragment.this.position = position;
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isEmpty()) {
                    new TaskService(getActivity()).save(new Task(
                            nameAdapter.getItem(position).getId(),
                            nameAdapter.getItem(position).getName(),
                            DateDialog.getDate(),
                            dateComplete.getText().toString(),
                            null,
                            topic.getText().toString(),
                            comment.getText().toString(),
                            null,
                            State.PERFORMED
                    ));
                    Transaction.back((ActionBarActivity) getActivity());
                    ToastManager.show(getActivity(), getString(R.string.toast_create_task));
                } else
                    ToastManager.show(getActivity(), getString(R.string.empty_field));
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Transaction.back((ActionBarActivity) getActivity());
            }
        });
    }

    @Override
    public void viewLogic() {
        nameAdapter = new NameAdapter(getActivity(), name);
        new TopicAdapter(getActivity(), topic);
        new DateDialog(getActivity(), dateComplete, getString(R.string.date_complete));
        EditParam.setParam(name, null);
        EditParam.setParam(dateComplete, null);
        EditParam.setParam(topic, 15);
        EditParam.setParam(comment, 100);
    }

    @Override
    public boolean isEmpty() {
        return name.getText().toString().equals("")
                || topic.getText().toString().equals("")
                || dateComplete.getText().toString().equals("")
                || comment.getText().toString().equals("");
    }



}
