package com.elrsoft.workflow.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.elrsoft.workflow.R;
import com.elrsoft.workflow.model.User;
import com.elrsoft.workflow.service.UserService;

/**
 * Created by Alex on 22.03.2015.
 */
public class UserAddFragment extends Fragment implements View.OnClickListener {

    EditText name, email, phone;

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_add, container, false);
        name = (EditText)view.findViewById(R.id.name);
        email = (EditText)view.findViewById(R.id.email);
        phone = (EditText)view.findViewById(R.id.phone);
        Button add = (Button)view.findViewById(R.id.add);
        Button cancel = (Button)view.findViewById(R.id.cancel);
        add.setOnClickListener(this);
        cancel.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add:
                UserService userService = new UserService(getActivity());
                userService.save(new User(name.getText().toString(), email.getText().toString(), phone.getText().toString()));
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.cancel:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }
    }
}
