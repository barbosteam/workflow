package com.elrsoft.workflow.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.elrsoft.workflow.R;
import com.elrsoft.workflow.activity.MainActivity;
import com.elrsoft.workflow.adapter.UserAdapter;
import com.elrsoft.workflow.fragments.core.Transaction;
import com.elrsoft.workflow.model.Task;
import com.elrsoft.workflow.model.User;
import com.elrsoft.workflow.service.TaskService;
import com.elrsoft.workflow.service.UserService;

import java.util.List;

/**
 * Created by Alex on 22.03.2015.
 */
public class UserListFragment extends Fragment {

    private final int USER_EDIT = 1;
    private final int USER_DELETE = 2;

    private ListView listView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_list, container, false);
        listView = (ListView) view.findViewById(R.id.user_list);
        UserAdapter adapter = new UserAdapter(new UserService(getActivity()).getAll(), getActivity());
        listView.setAdapter(adapter);
        registerForContextMenu(listView);
        return view;

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.user_list) {
            menu.add(0, USER_EDIT, 0, R.string.edit);
            menu.add(0, USER_DELETE, 0, R.string.delete);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        User user = (User)listView.getAdapter().getItem(acmi.position);
        switch (item.getItemId()) {
            case USER_EDIT:
                Transaction.showFragment((MainActivity) getActivity(), UserEditFragment.getInstance(user));
                return true;
            case USER_DELETE:
                UserService userService = new UserService(getActivity());
                TaskService taskService = new TaskService(getActivity());

                //delete user
                userService.delete(user);

                 //get all tasks for this user and delete all units with this user
                 List <Task> tasks = taskService.getAll();
                 for(int i = 1; i < tasks.size(); i++){
                 if(tasks.get(0).getIdUser() == user.getId()) {
                     taskService.delete(tasks.get(i).getId());
                    }
                 }

                  //delete tasks
                  for(Task temp: tasks) {
                  taskService.delete(temp);
                  }
                  listView.setAdapter(new UserAdapter(new UserService(getActivity()).getAll(), getActivity()));
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

}
