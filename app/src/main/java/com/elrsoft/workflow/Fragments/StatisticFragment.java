package com.elrsoft.workflow.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RadioButton;

import com.elrsoft.workflow.R;
import com.elrsoft.workflow.activity.MainActivity;
import com.elrsoft.workflow.adapter.drop_down.NameAdapter;
import com.elrsoft.workflow.db.resource.Resource;
import com.elrsoft.workflow.dialog.DateDialog;
import com.elrsoft.workflow.fragments.core.Initilization;
import com.elrsoft.workflow.fragments.core.Listeners;
import com.elrsoft.workflow.fragments.core.Logic;
import com.elrsoft.workflow.fragments.core.Transaction;
import com.elrsoft.workflow.service.TaskService;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by Alex on 22.03.2015.
 */
public class StatisticFragment extends Fragment implements Initilization, Logic, Listeners {

    private View view;
    private AutoCompleteTextView name, from, to;
    private Button find, cancel;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_statictic, container, false);

        initializationView();
        initializationListeners();
        viewLogic();

        return view;
    }

    @Override
    public void viewLogic() {
        new NameAdapter(getActivity(), name);
        new DateDialog(getActivity(), from, getResources().getString(R.string.from));
        new DateDialog(getActivity(), to, getResources().getString(R.string.to));
    }

    @Override
    public void initializationView() {
        name = (AutoCompleteTextView) view.findViewById(R.id.name);
        from = (AutoCompleteTextView) view.findViewById(R.id.from);
        to = (AutoCompleteTextView) view.findViewById(R.id.to);

        find = (Button) view.findViewById(R.id.find);
        cancel = (Button) view.findViewById(R.id.cancel);
    }

    @Override
    public void initializationListeners() {
        find.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinkedHashMap<String, String> args = new LinkedHashMap<>();
                if(!name.getText().toString().equals("")) {
                    args.put(Resource.Task.NAME, name.getText().toString());
                }
                if(!from.getText().toString().equals("")) {
                    args.put("FROM", from.getText().toString());
                }
                if(!to.getText().toString().equals("")) {
                    args.put("TO", to.getText().toString());
                }
                Transaction.showFragment((MainActivity)getActivity(),
                        CircleFragment.getInstance(new TaskService(getActivity()).getStatisticList(args)));
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Transaction.back((ActionBarActivity) getActivity());
            }
        });
    }

}
