package com.elrsoft.workflow.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.elrsoft.workflow.R;
import com.elrsoft.workflow.adapter.drop_down.NameAdapter;
import com.elrsoft.workflow.model.Task;
import com.elrsoft.workflow.model.User;
import com.elrsoft.workflow.service.TaskService;

/**
 * Created by Alex on 22.03.2015.
 */
public class TaskEditFragment extends Fragment implements View.OnClickListener {
    public final static String EXTRA_TASK = "com.elrsoft.workflow.task";

    private View view;
    //private AutoCompleteTextView name, dateComplete, topic, comment;
    private EditText name, dateComplete, topic, comment;
    private Button save, cancel;
    private NameAdapter nameAdapter;
    private int position;
    private User user;
    private Task task;

    public static TaskEditFragment getInstance(Task task) {
        TaskEditFragment fragment = new TaskEditFragment();
        fragment.task = task;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_task_edit, container, false);

        name = (EditText)view.findViewById(R.id.name);
        dateComplete = (EditText)view.findViewById(R.id.date_complete);
        topic = (EditText)view.findViewById(R.id.topic);
        comment = (EditText)view.findViewById(R.id.comment);

        name.setText(task.getName());
        dateComplete.setText(task.getDateComplete());
        topic.setText(task.getTopic());
        comment.setText(task.getCommentYou());

        Button save = (Button)view.findViewById(R.id.save);
        Button cancel = (Button)view.findViewById(R.id.cancel);
        save.setOnClickListener(this);
        cancel.setOnClickListener(this);

        return view;
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save:
                TaskService taskService = new TaskService(getActivity());
                task.setName(name.getText().toString());
                task.setDateComplete(dateComplete.getText().toString());
                task.setTopic(topic.getText().toString());
                task.setCommentYou(comment.getText().toString());
                taskService.update(task);
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.cancel:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }

    }
}
