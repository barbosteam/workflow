package com.elrsoft.workflow.fragments.core;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;

import com.elrsoft.workflow.R;

/**
 * Created by Alex on 22.03.2015.
 */
public class Transaction {

    public static void showFragment(ActionBarActivity activity, Fragment fragment){
        String backStateName = fragment.getClass().getName();

        FragmentManager mFragmentManager = activity.getSupportFragmentManager();
        boolean fragmentPopped = mFragmentManager.popBackStackImmediate(backStateName, 0);
        if (!fragmentPopped){
            FragmentTransaction ft = mFragmentManager.beginTransaction();
            ft.replace(R.id.container, fragment);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    public static void back(ActionBarActivity activity){
        FragmentManager mFragmentManager = activity.getSupportFragmentManager();
        mFragmentManager.popBackStack();
    }

}
