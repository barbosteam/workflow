package com.elrsoft.workflow.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.elrsoft.workflow.R;
import com.elrsoft.workflow.fragments.core.Initilization;
import com.elrsoft.workflow.fragments.core.Listeners;
import com.elrsoft.workflow.fragments.core.Transaction;

/**
 * Created by Alex on 22.03.2015.
 */
public class CircleFragment extends Fragment implements Initilization, Listeners {

    private static final String EXTRA_STATISTICS = "package com.elrsoft.workflow.statistics";
    private int[] statistics;

    private View view;
    private Button cancel;

    public static CircleFragment getInstance(int[] statistics) {
        CircleFragment fragment = new CircleFragment();
        Bundle args = new Bundle();
        args.putIntArray(EXTRA_STATISTICS, statistics);
        fragment.setArguments(args);
        Log.d("Debug", "Complete = " + statistics[0] + "   NotComplete = " + statistics[1] + "   Performed = " + statistics[2]);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        statistics = getArguments().getIntArray(EXTRA_STATISTICS);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_circle, container, false);

        initializationView();
        initializationListeners();

        return view;

    }

    @Override
    public void initializationView() {
        cancel = (Button)view.findViewById(R.id.cancel);
    }

    @Override
    public void initializationListeners() {
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Transaction.back((ActionBarActivity) getActivity());
            }
        });
    }
}