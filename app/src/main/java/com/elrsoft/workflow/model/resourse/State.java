package com.elrsoft.workflow.model.resourse;

import java.util.ArrayList;

/**
 * Created by Alex on 23.03.2015.
 */
public class State {
    public static final String COMPLETE = "complete";
    public static final String NOT_COMPLETE = "not_complete";
    public static final String PERFORMED = "performed";
    public static final ArrayList<String> STATE_LIST = new ArrayList<String>();;
    static {
        STATE_LIST.add(COMPLETE);
        STATE_LIST.add(NOT_COMPLETE);
        STATE_LIST.add(PERFORMED);
    }
}
