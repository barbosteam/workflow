package com.elrsoft.workflow.model.resourse;

/**
 * Created by Alex on 27.03.2015.
 */
public class Filter {
    public static final String NAME = "name";
    public static final String TOPIC = "topic";
    public static final String STATE = "state";
    public static final String DATE = "date";
}
