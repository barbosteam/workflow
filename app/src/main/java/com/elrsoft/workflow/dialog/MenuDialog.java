package com.elrsoft.workflow.dialog;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.elrsoft.workflow.R;

/**
 * Created by Alex on 31.03.2015.
 */
public class MenuDialog extends DialogFragment implements View.OnClickListener {

    TextView txtEdit, txtDelete, txtCancel;

    OnFinishDialog onFinishDialog;

    public interface OnFinishDialog {
        public void isOnFinishDialog();
    }

    public void setOnFinishDialog(OnFinishDialog onFinishDialog) {
        this.onFinishDialog = onFinishDialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_menu, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        txtEdit = (TextView) view.findViewById(R.id.txtEdit);
        txtDelete = (TextView) view.findViewById(R.id.txtDelete);
        txtCancel = (TextView) view.findViewById(R.id.txtCancel);
        txtEdit.setOnClickListener(this);
        txtDelete.setOnClickListener(this);
        txtCancel.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtEdit:
                break;
            case R.id.txtDelete:
                break;
            case R.id.txtCancel:
                break;

        }
        getDialog().dismiss();
    }


}