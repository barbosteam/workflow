package com.elrsoft.workflow.dialog;

import android.app.DatePickerDialog;
import android.content.Context;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * Created by Alex on 28.03.2015.
 */
public class DateDialog {

    private Context context;
    private AutoCompleteTextView autoCompleteTextView;
    private String title;

    public DateDialog(Context context, AutoCompleteTextView autoCompleteTextView, String title){
        this.context = context;
        this.autoCompleteTextView = autoCompleteTextView;
        this.title = title;
        setShowDialogListener(autoCompleteTextView);
    }

    private void showDateDialog(){
        DatePickerDialog mDatePicker;
        Calendar mcurrentTime = Calendar.getInstance();
        int year = mcurrentTime.get(Calendar.YEAR);
        int month = mcurrentTime.get(Calendar.MONTH);
        int day = mcurrentTime.get(Calendar.DAY_OF_MONTH);

        mDatePicker = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String dayOfMonthS = String.valueOf(dayOfMonth);
                String monthOfYearS = String.valueOf(monthOfYear + 1);
                if (dayOfMonthS.length() == 1) dayOfMonthS = "0" + dayOfMonthS;
                if (monthOfYearS.length() == 1) monthOfYearS = "0" + monthOfYearS;
                autoCompleteTextView.setText(dayOfMonthS + "." + monthOfYearS + "." + year);
            }
        }, year, month, day);
        mDatePicker.setButton(DatePickerDialog.BUTTON_POSITIVE, "Застосувати", mDatePicker);
        mDatePicker.setButton(DatePickerDialog.BUTTON_NEGATIVE, "", mDatePicker);
        mDatePicker.setTitle(title);
        mDatePicker.show();
    }

    public static String getDate(){
        Calendar mcurrentTime = Calendar.getInstance();
        int year = mcurrentTime.get(Calendar.YEAR);
        String dayOfMonthS = String.valueOf(mcurrentTime.get(Calendar.DAY_OF_MONTH));
        String monthOfYearS = String.valueOf(mcurrentTime.get(Calendar.MONTH) + 1);
        if (dayOfMonthS.length() == 1) dayOfMonthS = "0" + dayOfMonthS;
        if (monthOfYearS.length() == 1) monthOfYearS = "0" + monthOfYearS;
        return dayOfMonthS + "." + monthOfYearS + "." + year;
    }

    public void setShowDialogListener(AutoCompleteTextView a){
        final AutoCompleteTextView simple = a;

        a.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus){
                    showDateDialog();
                }
            }
        });
        a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialog();
            }
        });
    }

}
