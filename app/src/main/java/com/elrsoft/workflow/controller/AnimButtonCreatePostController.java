package com.elrsoft.workflow.controller;

import android.view.animation.Animation;
import android.widget.ImageButton;

/**
 * Created by Alex on 31.03.2015.
 */
public class AnimButtonCreatePostController {
    private ImageButton button;
    private boolean finish = true;

    public AnimButtonCreatePostController(ImageButton button) {
        this.button = button;
    }

    public void startAnim(Animation animation, final int visibl) {
        if (visibl != button.getVisibility() && finish) {
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    finish = false;
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    button.setVisibility(visibl);
                    finish = true;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            button.startAnimation(animation);
        }

    }
}