package com.elrsoft.workflow.controller;

import android.os.Bundle;

import com.elrsoft.workflow.model.Task;

import java.util.ArrayList;

/**
 * Created by Alex on 31.03.2015.
 */
public class BundleManagerController {

    public static Bundle createBundleWithMandate(Task task, String key) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(key, task);
        return bundle;
    }

    public static Bundle createBundleWithMandateList(ArrayList<Task> tasks, String key) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(key, tasks);
        return bundle;
    }
}
