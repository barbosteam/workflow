package com.elrsoft.workflow.list;

import com.elrsoft.workflow.model.Task;
import com.elrsoft.workflow.model.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Alex on 30.03.2015.
 */
public class NoDuplicatesList<E> extends LinkedList<E> {

    public NoDuplicatesList(){

    }

    public NoDuplicatesList(Collection<? extends E> collection){
        Collection<E> copy = new LinkedList<E>(collection);
        copy.removeAll(this);
        super.addAll(copy);
    }

    @Override
    public boolean add(E e) {
        if (this.contains(e)) {
            return false;
        }
        else {
            return super.add(e);
        }
    }

    @Override
    public boolean addAll(Collection<? extends E> collection) {
        Collection<E> copy = new LinkedList<E>(collection);
        copy.removeAll(this);
        return super.addAll(copy);
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> collection) {
        Collection<E> copy = new LinkedList<E>(collection);
        copy.removeAll(this);
        return super.addAll(index, copy);
    }

    @Override
    public void add(int index, E element) {
        if (this.contains(element)) {
            return;
        }
        else {
            super.add(index, element);
        }
    }

    public static ArrayList<String> getTopicList(List<Task> tasks){
        NoDuplicatesList<String> topic = new NoDuplicatesList<String>();
        for (int i = 0; i < tasks.size(); i++){
            topic.add(tasks.get(i).getTopic());
        }
        return new ArrayList<String>(topic);
    }

    public static ArrayList<String> getNameList(List<User> users){
        NoDuplicatesList<String> name = new NoDuplicatesList<String>();
        for (int i = 0; i < users.size(); i++){
            name.add(users.get(i).getName());
        }
        return new ArrayList<String>(name);
    }
}
