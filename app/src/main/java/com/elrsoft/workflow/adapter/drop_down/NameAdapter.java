package com.elrsoft.workflow.adapter.drop_down;

import android.content.Context;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;

import com.elrsoft.workflow.list.NoDuplicatesList;
import com.elrsoft.workflow.model.User;
import com.elrsoft.workflow.service.UserService;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;

import java.util.List;

/**
 * Created by Alex on 30.03.2015.
 */
public class NameAdapter extends DropDown {
    private List<User> list;

    public NameAdapter(Context context, AutoCompleteTextView view) {
        super(context, NoDuplicatesList.getNameList(new UserService(context).getAll()), view);
    }

    public NameAdapter(Context context, MaterialAutoCompleteTextView view){
        super(context, NoDuplicatesList.getNameList(new UserService(context).getAll()), view);
        list = new UserService(context).getAll();
    }

    public NameAdapter(Context context, AutoCompleteTextView view, CheckBox check){
        super(context, NoDuplicatesList.getNameList(new UserService(context).getAll()), view, check);
    }

    public User getItem(int position){
        return list.get(position);
    }

}
