package com.elrsoft.workflow.adapter;

import android.animation.ValueAnimator;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.elrsoft.workflow.R;
import com.elrsoft.workflow.db.async.MainFragmentAsync;
import com.elrsoft.workflow.fragments.MainFragment;
import com.elrsoft.workflow.fragments.TaskEditFragment;
import com.elrsoft.workflow.fragments.core.Transaction;
import com.elrsoft.workflow.model.Task;
import com.elrsoft.workflow.model.resourse.State;
import com.elrsoft.workflow.service.TaskService;

import java.util.List;

/**
 * Created by Alex on 31.03.2015.
 */
public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.ViewHolder> {

    LayoutInflater inflater;
    List<Task> list;
    OnItemLongClickListener onItemClickListener;
    private ViewHolder holder;
    MainFragment mainFragment;

    public FilterAdapter(MainFragment mainFragment, List<Task> list) {
        this.mainFragment = mainFragment;
        this.list = list;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.from(parent.getContext()).inflate(R.layout.adapter_filter, null, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        this.holder = holder;
        Task task = list.get(position);
        if (task != null) {
            String state = task.getState();
            if (state.equals(State.COMPLETE))
                holder.circle.setBackgroundResource(R.drawable.circle_complete);
            if (state.equals(State.NOT_COMPLETE))
                holder.circle.setBackgroundResource(R.drawable.circle_not_complete);
            if (state.equals(State.PERFORMED))
                holder.circle.setBackgroundResource(R.drawable.circle_perfomed);

            holder.name.setText(task.getName());
            holder.topic.setText(task.getTopic());
            holder.dateCreate.setText(task.getDateCreate());
            holder.dateComplete.setText(task.getDateComplete());
            holder.dateFinish.setText(task.getDateFinish());
            holder.comment.setText(task.getCommentYou());



        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public Task getTaskId(int position) {
        return list.get(position);
    }

    public void setOnItemClickListener(OnItemLongClickListener itemClickListener) {
        onItemClickListener = itemClickListener;
    }

    public int getPosition(){
        return holder.position;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener,
    View.OnCreateContextMenuListener{

        private int originalHeight = 0;
        private int animHeight = 0;
        private boolean isViewExpanded = false;
        boolean mIsViewExpanded = false;
        boolean check = false;
        private int position = 0;

        TextView name, dateFinish, dateComplete, dateCreate, topic,  comment;
        LinearLayout conteinerAnim, nameLayout;
        CardView card_view;
        View circle;


        public ViewHolder(final View itemView) {
            super(itemView);

            card_view = (CardView) itemView.findViewById(R.id.card_view);
            name = (TextView) itemView.findViewById(R.id.name);
            topic = (TextView) itemView.findViewById(R.id.topic);
            dateCreate = (TextView) itemView.findViewById(R.id.date_create);
            dateComplete = (TextView) itemView.findViewById(R.id.date_complete);
            dateFinish = (TextView) itemView.findViewById(R.id.date_finish);
            comment = (TextView) itemView.findViewById(R.id.comment);

            nameLayout = (LinearLayout) itemView.findViewById(R.id.linearLayoutOne);
            circle = (View) itemView.findViewById(R.id.circle);
            conteinerAnim = (LinearLayout) itemView.findViewById(R.id.container_anim);

            itemView.setOnLongClickListener(this);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (originalHeight == 0) {
                        originalHeight = itemView.getHeight();
                    }
                    ValueAnimator valueAnimator;
                    if (!mIsViewExpanded) {
                        conteinerAnim.setVisibility(View.VISIBLE);
                        conteinerAnim.setEnabled(true);
                        mIsViewExpanded = true;
                        valueAnimator = ValueAnimator.ofInt(originalHeight, originalHeight + animHeight);
                    } else {

                        mIsViewExpanded = false;
                        valueAnimator = ValueAnimator.ofInt(originalHeight + animHeight, originalHeight);

                        Animation a = new AlphaAnimation(1.00f, 0.00f);

                        a.setDuration(200);
                        a.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                conteinerAnim.setVisibility(View.INVISIBLE);
                                conteinerAnim.setEnabled(false);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });

                        conteinerAnim.startAnimation(a);
                    }
                    valueAnimator.setDuration(200);
                    valueAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
                    valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        public void onAnimationUpdate(ValueAnimator animation) {
                            Integer value = (Integer) animation.getAnimatedValue();
                            itemView.getLayoutParams().height = value;
                            itemView.requestLayout();
                        }
                    });


                    valueAnimator.start();
                }
            });

            if (!isViewExpanded) {

                conteinerAnim.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                    @Override
                    public void onGlobalLayout() {
                        if (!check) {
                            animHeight = conteinerAnim.getHeight();
                            conteinerAnim.setVisibility(View.GONE);
                            //conteinerAnim.setEnabled(false);
                            check = true;
                        }
                    }
                });
            }

        }

        @Override
        public boolean onLongClick(View v) {
            v.setOnCreateContextMenuListener(this);
            v.showContextMenu();

            return true;
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

            MenuItem edit = menu.add(0, 1, 0, mainFragment.getActivity().getString(R.string.edit));
            MenuItem delete = menu.add(0, 2, 0, mainFragment.getActivity().getString(R.string.delete));
            edit.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    Transaction.showFragment((ActionBarActivity) mainFragment.getActivity(),
                            TaskEditFragment.getInstance(list.get(getPosition())));
                    return true;
                }
            });
            delete.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    new TaskService(mainFragment.getActivity()).delete(list.get(getPosition()).getId());
                    new MainFragmentAsync(mainFragment).execute();
                    return true;
                }
            });
        }

    }

    public interface OnItemLongClickListener {
        public void onLongClick(Task task);
    }
}
