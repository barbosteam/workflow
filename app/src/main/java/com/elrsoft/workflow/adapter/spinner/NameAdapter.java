package com.elrsoft.workflow.adapter.spinner;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.elrsoft.workflow.model.User;
import com.elrsoft.workflow.service.UserService;

import java.util.List;

/**
 * Created by Alex on 31.03.2015.
 */
public class NameAdapter extends BaseAdapter {

    private Context context;
    private List<User> userList;

    public NameAdapter(Context context){
        userList = new UserService(context).getAll();
    }

    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public User getItem(int position) {
        return userList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return null;
    }
}
