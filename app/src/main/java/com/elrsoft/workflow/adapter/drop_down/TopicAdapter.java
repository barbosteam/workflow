package com.elrsoft.workflow.adapter.drop_down;

import android.content.Context;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;

import com.elrsoft.workflow.list.NoDuplicatesList;
import com.elrsoft.workflow.service.TaskService;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;

/**
 * Created by Alex on 30.03.2015.
 */
public class TopicAdapter extends DropDown {

    public TopicAdapter(Context context, MaterialAutoCompleteTextView view){
        super(context, NoDuplicatesList.getTopicList(new TaskService(context).getAll()), view);
    }

    public TopicAdapter(Context context, AutoCompleteTextView view, CheckBox check){
        super(context, NoDuplicatesList.getTopicList(new TaskService(context).getAll()), view, check);
    }

}
