package com.elrsoft.workflow.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.elrsoft.workflow.R;
import com.elrsoft.workflow.fragments.FilterFragment;
import com.elrsoft.workflow.fragments.MainFragment;
import com.elrsoft.workflow.fragments.StatisticFragment;
import com.elrsoft.workflow.fragments.TaskCreateFragment;
import com.elrsoft.workflow.fragments.UserAddFragment;
import com.elrsoft.workflow.fragments.UserListFragment;
import com.elrsoft.workflow.fragments.core.Transaction;
import com.elrsoft.workflow.service.TaskService;
import com.elrsoft.workflow.service.UserService;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        UserService userDao = new UserService(this);
        TaskService taskDao = new TaskService(this);
/*
        userDao.save(new User("Alex", "alexoleg1993@gmail.com", "+380668147810"));
        userDao.save(new User("Andy", "alexoleg1993@gmail.com", "+380668147810"));
        userDao.save(new User("Mary", "alexoleg1993@gmail.com", "+380668147810"));
        userDao.save(new User("Kate", "alexoleg1993@gmail.com", "+380668147810"));
        userDao.save(new User("Ann", "alexoleg1993@gmail.com", "+380668147810"));
        userDao.save(new User("Serg", "alexoleg1993@gmail.com", "+380668147810"));

        taskDao.save(new Task("23.03.2015", "23.03.2015", "23.03.2015", "Topic", "Comment", null, "State"));
        taskDao.save(new Task("23.03.2015", "23.03.2015", "23.03.2015", "Topic", "Comment", null, "State"));
        taskDao.save(new Task("23.03.2015", "23.03.2015", "23.03.2015", "Topic", "Comment", null, "State"));
        taskDao.save(new Task("23.03.2015", "23.03.2015", "23.03.2015", "Topic", "Comment", null, "State"));
        taskDao.save(new Task("23.03.2015", "23.03.2015", "23.03.2015", "Topic", "Comment", null, "State"));
        taskDao.save(new Task("23.03.2015", "23.03.2015", "23.03.2015", "Topic", "Comment", null, "State"));
        taskDao.save(new Task("23.03.2015", "23.03.2015", "23.03.2015", "Topic", "Comment", null, "State"));
        taskDao.save(new Task("23.03.2015", "23.03.2015", "23.03.2015", "Topic", "Comment", null, "State"));


        unitDao.save(new Unit(1, 1));
        unitDao.save(new Unit(2, 2));
        unitDao.save(new Unit(3, 3));
        unitDao.save(new Unit(4, 4));
        unitDao.save(new Unit(5, 5));
        unitDao.save(new Unit(6, 6));
        unitDao.save(new Unit(1, 7));
        unitDao.save(new Unit(2, 8));

        */

/*
        for (int i = 1; i < 40; i++){
            taskDao.save(new Task(1, "Alex", "23.03.2015", "23.03.2015",
                    "23.03.2015", "Topic" + i, "Comment", null, State.PERFORMED));
            taskDao.save(new Task(1, "Alex", "23.03.2015", "23.03.2015",
                    "23.03.2015", "Topic" + i, "Comment", null, State.COMPLETE));
            taskDao.save(new Task(1, "Alex", "23.03.2015", "23.03.2015",
                    "23.03.2015", "Topic" + i, "Comment", null, State.NOT_COMPLETE));
        }
*/



        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.container,
                    new MainFragment()).commit();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.create_new_task) {
            Transaction.showFragment(MainActivity.this, new TaskCreateFragment());
            return false;
        }
        if (id == R.id.create_new_user) {
            Transaction.showFragment(MainActivity.this, new UserAddFragment());
            return false;
        }
        if (id == R.id.user_list) {
            Transaction.showFragment(MainActivity.this, new UserListFragment());
            return false;
        }
        if (id == R.id.statistic) {
            Transaction.showFragment(MainActivity.this, new StatisticFragment());
            return false;
        }
        if (id == R.id.filter) {
            Transaction.showFragment(MainActivity.this, new FilterFragment());
            return false;
        }
        if (id == R.id.exit) {
            finish();
            return false;
        }

        return super.onOptionsItemSelected(item);
    }

}
